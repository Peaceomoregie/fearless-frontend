import React, {useEffect, useState } from 'react';

function ConferenceForm () {

    // Fetch data to get all location options
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);



    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handlePresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const handleAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.name = name
        data.starts = starts
        data.ends = ends
        data.description = description
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.location = location
        const json = JSON.stringify(data)
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchOptions = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(conferenceUrl, fetchOptions)
        if (response.ok) {
            const newConference = await response.json()

            setName('')
            setStarts('')
            setEnds('')
            setDescription('')
            setMaxPresentations('')
            setMaxAttendees('')
            setLocation('')
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" value={name} name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} placeholder="Starts" required type="date" value={starts} name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} placeholder="Ends" required type="date" value={ends} name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescriptionChange} className="form-control" value={description} name="description" id="description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentationsChange} placeholder="Maximum Presentations" required type="number" value={maxPresentations} name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendeesChange} placeholder="Maximum Attendees" required type="number" value={maxAttendees} name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select required onChange={handleLocationChange} id="location" value={location} name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.href} value={location.id} >
                            {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm