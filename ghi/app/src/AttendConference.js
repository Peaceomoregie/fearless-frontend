import React, {useEffect, useState } from 'react';

function AttnedConference() {
    // Fetch data to get all location options
    const [conferences, setConferences] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    const [conference, setConference] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.name = name
        data.conference = conference
        data.email = email
        const json = JSON.stringify(data)
        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchOptions = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(attendeeUrl, fetchOptions)
        if (response.ok) {
            const newAttendee = await response.json()

            setName('')
            setConference('')
            setEmail('')
        }
    }

    
    return (
      <div className="attendContainer">
        <div className="logoContainer">
            <div className="logo shadow mt-4">
                <img src="/logo.svg"/>
            </div>

        </div>
        <div className="w-100">
          <div className="shadow p-4 mt-4">
            <h1>It's Conference Time!</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="mb-3 w-100">
                <label htmlFor="conference">Please choose which conference you'd like to attend.</label>
                <select onChange={handleConferenceChange} required value={conference} id="state" name="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                        return (
                            //"This is one of those React things.
                            // Whenever you use the map method, you have to add a key so that
                            // React can keep track of the things it's generating. If you give it a value unique to the
                            // property, the error should go away."
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                        );
                    })}
                </select>
              </div>
              Now, tell us about yourself.
              <div className="attendee">
                <div className="form-floating mb-3 w-50">
                    <label htmlFor="name">Your full name</label>
                    <input onChange={handleNameChange} placeholder="Your full name" value={name} required type="text" name="name" id="name" className="form-control"/>
                </div>
                <div className="form-floating mb-3 w-50">
                    <input onChange={handleEmailChange} placeholder="email" value={email} required type="email" name="email" id="email" className="form-control"/>
                    <label htmlFor="email">Your email address</label>
                </div>
              </div>
              <button className="btn btn-primary">I'm going!</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default AttnedConference