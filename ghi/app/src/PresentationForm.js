import React, {useEffect, useState } from 'react';

function PresentationForm () {
    // Fetch data to get all location options
    const [conferences, setConferences] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    const [presenterName, setPresenterName] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }
    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }



    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.presenter_name = presenterName
        data.company_name = companyName
        data.presenter_email = presenterEmail
        data.title = title
        data.synopsis = synopsis
        const json = JSON.stringify(data)
        const presentationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchOptions = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(presentationUrl, fetchOptions)
        if (response.ok) {
            const newPresentation = await response.json()

            setPresenterName('')
            setCompanyName('')
            setPresenterEmail('')
            setTitle('')
            setSynopsis('')
            setConference('')
        }
    }


    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange} placeholder="Presenter name" required type="text" value={presenterName} name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresenterEmailChange} placeholder="Presenter email" required type="email" value={presenterEmail} name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company name" required type="text" value={companyName} name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" value={title} id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis" className="form-label">Synopsis</label>
                <textarea required onChange={handleSynopsisChange} className="form-control" id="synopsis" value={synopsis} name="synopsis" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} required id="conference" value={conference} name="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                        return (
                            //"This is one of those React things.
                            // Whenever you use the map method, you have to add a key so that
                            // React can keep track of the things it's generating. If you give it a value unique to the
                            // property, the error should go away."
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default PresentationForm