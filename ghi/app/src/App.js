import Nav from './Nav';
import { Link, Routes, Route, Router } from "react-router-dom";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttnedConference from './AttendConference';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (

      <div className="containerr">
        <Nav />
        <Routes>
        <Route path="/" element={<MainPage />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route path="/attendees/new" element={<AttnedConference />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/attendee/list" element={<AttendeesList attendees={props.attendees}/>} />
        </Routes>
      </div>

  );
}

export default App;
