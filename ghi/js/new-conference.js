window.addEventListener("DOMContentLoaded", async()  => {
    //Get States for drop down
    const url = 'http://localhost:8000/api/locations/'
    const response = await fetch(url)
    if (response.ok) {
        const locationList = await response.json();
        const selector = document.getElementById('location');

        for (let location of locationList.locations) {
            const option = document.createElement('option');
            //This gets the id number from the href string
            option.value = location.href[location.href.length - 2 ];
            option.innerHTML = location.name;
            selector.appendChild(option)
        }

    }



    //Even Listener for when a form is sumbitted
    const form = document.getElementById('create-conference-form');
    form.addEventListener('submit', async (event) => {
        //prevents the default behavior of the get request & reload
        event.preventDefault();
        const formData = new FormData(form);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';

        //Fetch options object
        const fetchOptions = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(conferenceUrl, fetchOptions);
        if (response.ok) {
            form.reset();
            const newConference = await response.json();
            console.log(newConference)
        }

    });

});