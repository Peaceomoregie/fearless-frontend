
window.addEventListener("DOMContentLoaded", async()  => {
    //Get States for drop down
    const url = 'http://localhost:8000/api/states/'
    const response = await fetch(url)
    if (response.ok) {
        const stateList = await response.json();
        const selector = document.getElementById('state');

        for (let state of stateList.states) {
            const option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selector.appendChild(option)
        }

    }



    //Even Listener for when a form is sumbitted
    const form = document.getElementById('create-location-form');
    form.addEventListener('submit', async (event) => {
        //prevents the default behavior of the get request & reload
        event.preventDefault();
        const formData = new FormData(form);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';

        //Fetch options object
        const fetchOptions = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(locationUrl, fetchOptions);
        if (response.ok) {
            form.reset();
            const newLocation = await response.json();
            
        }

    });

});
