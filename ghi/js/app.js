window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      return alert(
        `${response.status}: ${response.url} ${response.statusText}`
        ) ;
    } else {
      const data = await response.json();
      console.log(url)
      function createCard(name, description, pictureUrl, location, starts, ends) {
        const startDate = new Date(starts).toLocaleDateString();
        const endDate = new Date(ends).toLocaleDateString();
        return `
          <div class="col shadow">
            <div class="card">
              <img src="${pictureUrl}" class="card-img-top">
              <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
              </div>
              <div class="card-footer">
                ${startDate} - ${endDate}
              </div>
            </div>
          </div>
        `;
      }

      let i = 1;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name;
          const starts = details.conference.starts;
          const ends = details.conference.ends
          const html = createCard(title, description, pictureUrl, location, starts, ends);
          let column = document.querySelector('.column' + [i]);
          column.innerHTML += html;
          if (i === 3){
            i = 0
          }
          i += 1

        }
      }

    }
  } catch (e) {
    // Figure out what to do if an error is raised
    alert(e.message)
  }

  function alert(message) {
    const alertPlaceholder = document.getElementById("liveAlertPlaceholder");
    const wrapper = document.createElement("div");
    wrapper.innerHTML = `
      <div class="alert alert-danger alert-dismissible" role="alert">
        ${message}
        <button type="button" class="btn-close" data-bs-dismiss="aler" aria-label="Close"></button>
      </div>`;

    alertPlaceholder.append(wrapper);
  }
});
