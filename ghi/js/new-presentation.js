window.addEventListener("DOMContentLoaded", async()  => {
    //Get conference for drop down
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const conferenceResponse = await fetch(conferenceUrl);
    const selector = document.getElementById('conference');

    if (conferenceResponse.ok) {
        const conferenceList = await conferenceResponse.json();

        for (let conference of conferenceList.conferences) {
            const option = document.createElement('option');
            option.value = conference.href
            option.innerHTML = conference.name;
            selector.appendChild(option)
        }
    }


    //Even Listener for when a form is sumbitted
    const form = document.getElementById('create-presentation-form');
    form.addEventListener('submit', async (event) => {
        //prevents the default behavior of the get request & reload
        event.preventDefault();
        const formData = new FormData(form);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = `http://localhost:8000${selector.value}presentations/`;

        //Fetch options object
        const fetchOptions = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(conferenceUrl, fetchOptions);
        if (response.ok) {
            form.reset();
            const newPresentation = await response.json();
        }

    });

});